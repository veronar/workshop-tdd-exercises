import unittest # <1>

def rev_validate(str):
    if len(str) == 0:
        return ""
    if len(str) == 1:
        return str


class MyTestCase(unittest.TestCase): # <2>

    def test_correct(self): # <3>
        """
        Tests if function called with "abcde" as parameter that it returns "edcba" as result
        """
        self.fail('Test not implemented yet') # <4>

    def test_empty(self):
        """
        Tests if function called with empty parameter that it does not break
        """
        self.assertEqual(rev_validate(""), "")

    def test_none(self):
        """
        Tests if function called with None as parameter that it does not break
        """
        self.fail('Test not implemented yet')

    def test_one_char(self):
        """
        Tests if function called with 1 character string as parameter that it does not break
        """
        self.assertEqual(rev_validate("a"), "a")

    def test_uniform_string(self):
        """
        Tests if function called with uniform string, e.g. "aaaa", as parameter that it does not break
        """
        self.fail('Test not implemented yet')



if __name__ == '__main__':
    unittest.main()