#!/bin/bash
echo 'Welcome to WTC_ TDD Workshop'

# Do pip install
echo 'Installing Python dependencies'
pip install -r .wtc/requirements.txt

# Setup watchman triggers
echo 'Configuring watchman'
#watchman -- trigger . exercises '**/*.py' -- python3 .wtc/run_tests.py
watchman -j <<-EOT
["trigger", ".", {
  "name": "exercises",
  "expression": ["anyof", ["match", "**/*.py", "wholename"]],
  "append_files": false,
  "command": ["python3", ".wtc/run_tests.py"]
}]
EOT

