import unittest # <1>

class MyTestCase(unittest.TestCase): # <2>

    def test_correct(self): # <3>
        self.assertEqual( 1+2, 3) # <4>


if __name__ == '__main__': # <5>
    unittest.main()
