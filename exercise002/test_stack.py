import unittest


class MyTestCase(unittest.TestCase):
    def test_empty(self):
        """
        Tests if an empty stack behaves as expected:
        * has length zero
        """
        self.fail('Test not implemented yet')

    def test_push_one(self):
        """
        Tests if we start with empty stack and push 1 item, that size is 1
        """
        self.fail('Test not implemented yet')

    def test_push_and_pop_one(self):
        """
        Tests if we start with empty stack and push 1 item, that a pop returns that item
        """
        self.fail('Test not implemented yet')

    def test_push_several(self):
        """
        Tests if we start with empty stack and push several items, that size == number of pushes
        """
        self.fail('Test not implemented yet')

    def test_push_pop_several(self):
        """
        Tests if we start with empty stack and push several items, that pop will return items in reversed order
        """
        self.fail('Test not implemented yet')

    def test_pop_empty(self):
        """
        Tests if we start with empty stack and try to pop, it raise an appropriate error
        """
        self.fail('Test not implemented yet')


if __name__ == '__main__':
    unittest.main()
